"use strict"

var carousel = document.querySelector('.carousel')
var style = document.documentElement.style
var trnsf = typeof style.transform == 'string' ? 'transform' : 'WebkitTransform'

var slider = new Flickity(carousel, {
    freeScroll: false,
    wrapAround: true,
    cellSelector: '.carousel-cell',
    selectedAttraction: 0.05,
    friction: 0.5,
    pageDots: true,
    cellAlign: 'center',
    imagesLoaded: true,
});

window.onresize = function(event) { slider.resize() };
